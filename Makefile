ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

#############################
# Create new project
#############################

help:
	bash help.sh $(ARGS)

###############################
# Initialize for Docker for Mac
###############################

docker:
	bash docker.sh $(ARGS)

#############################
# Argument fix workaround
#############################
%:
	@: